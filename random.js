function fetchRandomNumbers() {
  return new Promise((resolve, reject) => {
    console.log('Fetching number...')

    setTimeout(() => {
      let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0

      resolve(randomNum)
      console.log('Received random number:', randomNum)
    }, (Math.floor(Math.random() * 5) + 1) * 1000)
  })
}

function fetchRandomString(callback) {
  return new Promise((resolve, reject) => {
    console.log('Fetching string...')
    setTimeout(() => {
      let result = ''

      let characters =
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

      let charactersLength = characters.length

      for (let i = 0; i < 5; i++) {
        result += characters.charAt(
          Math.floor(Math.random() * charactersLength)
        )
      }

      resolve(result)
      console.log('Received random string:', result)
    }, (Math.floor(Math.random() * 5) + 1) * 1000)
  })
}

// Task 1

async function fetchNumString() {
  const randomNum = await fetchRandomNumbers()
  console.log(randomNum)
  const randomString = await fetchRandomString()
  console.log(randomString)
}

fetchNumString()

// Task 2

async function fetchTwoRandomNum() {
  let sum = 0
  const randomNum = await fetchRandomNumbers()
  sum += randomNum
  const randomNum2 = await fetchRandomNumbers()
  sum += randomNum2
  console.log(sum)
}
fetchTwoRandomNum()

// Task 3

async function concateNumString() {
  const randomNum = await Promise.all([
    fetchRandomNumbers(),
    fetchRandomString(),
  ])

  const concat = randomNum.reduce((acu, cur) => acu + cur, '')
  console.log('Concatenated string: ', concat)
}
concateNumString()

// Task 4

async function fetchTenRandomNum() {
  const randomNum = await Promise.all([
    fetchRandomNumbers(),
    fetchRandomNumbers(),
    fetchRandomNumbers(),
    fetchRandomNumbers(),
    fetchRandomNumbers(),
    fetchRandomNumbers(),
    fetchRandomNumbers(),
    fetchRandomNumbers(),
    fetchRandomNumbers(),
    fetchRandomNumbers(),
  ])

  const sum = randomNum.reduce((acu, cur) => acu + cur, 0)
  console.log('Sum: ', sum)
}
fetchTenRandomNum()
